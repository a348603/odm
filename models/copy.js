const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _number: Number,
    _movie: {
        type: mongoose.Schema.ObjectId,
        ref: 'Movie'
    }
});

class Copy {
    constructor(number, movie){
        this._number = number;
        this._movie = movie;
    }

    get number(){ 
        return this._number; 
    }

    set number(v){ 
        this._number = v; 
    }

    get movie(){ 
        return this._movie; 
    }
    set movie(v){ 
        this._movie = v; 
    }
}

schema.loadClass(Copy);
module.exports = mongoose.model('Copy', schema);