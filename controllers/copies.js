const express = require('express');
const Copy = require('../models/copy');
const Movie = require('../models/movie');

async function create(req, res, next){
    const number = req.body.number;
    const movieId = req.body.movieIdId;

    let movie = await Movie.findOne({"_id":movieId});
    let copy = new Copy({
        number: number,
        movie: movie,
    });

    copy.save().then(obj => res.status(200).json({
        msg: "Copia almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la copia",
        obj: ex
    }));
}

function list(req, res, next) {
    Copy.find().populate("_movie").then(objs => res.status(200).json({
        msg: "Lista de copias",
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: "No se pudo obtener la lista de copias",
        obj: ex
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Copy.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: `Copia con el id ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo consultar la copia",
        obj: ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let number = req.body.number ? req.body.number : "";
    let movie = req.body.movie ? req.body.movie : "";
    
    let copy = new Object({
        _number: number,
        _movie: movie,
    });
    
    Copy.findOneAndUpnumber({"_id":id}, copy, {new:true})
            .then(obj => res.status(200).json({
                msg: "Copia reemplazada correctamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo reemplazar la copia",
                obj: ex
            }));
}

function upnumber(req, res, next){
    const id = req.params.id;
    let number = req.body.number;
    let movie = req.body.movie;
    
    let copy = new Object();
    if(number) copy._number = number;
    if(movie) copy._movie = movie;
    
    Copy.findOneAndUpnumber({"_id":id}, copy)
            .then(obj => res.status(200).json({
                msg: "Copia actualizada corretamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo actualizar la copia",
                obj: ex
            }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Copy.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg: "Copia eliminada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo eliminar la copia",
        obj: ex
    }));
}

module.exports = {
    create, list, index, replace, upnumber, destroy
};