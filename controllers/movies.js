const express = require('express');
const Director = require('../models/director');
const Movie = require('../models/movie')

async function create(req, res, next){
    const title = req.body.title;
    const directorId = req.body.directorId;

    let director = await Director.findOne({"_id":directorId});
    let movie = new Movie({
        title: title,
        director: director
    });

    movie.save().then(obj => res.status(200).json({
        msg: "Pelicula almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la pelicula",
        obj: ex
    }));
}

function list(req, res, next) {
    Movie.find().populate("_director").then(objs => res.status(200).json({
        msg: "Lista de peliculas",
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: "No se pudo obtener la pelicula",
        obj: ex
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Movie.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: `Pelicula con el id ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo consultar la pelicula",
        obj: ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let title = req.body.title ? req.body.title : "";
    let director = req.body.director ? req.body.director : "";
    
    let movie = new Object({
        _title: title,
        _director: director
    });
    
    Movie.findOneAndUpdate({"_id":id}, movie, {new:true})
            .then(obj => res.status(200).json({
                msg: "Pelicula reemplazada correctamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo reemplazar la pelicula",
                obj: ex
            }));
}

function update(req, res, next){
    const id = req.params.id;
    let title = req.body.title;
    let director = req.body.director;
    
    let movie = new Object();
    if(title) title._title = title;
    if(director) director._director = director;
    
    Movie.findOneAndUpdate({"_id":id}, movie)
            .then(obj => res.status(200).json({
                msg: "Pelicula actualizada corretamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo actualizar la pelicula",
                obj: ex
            }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Movie.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg: "Pelicula eliminada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo eliminar la pelicula",
        obj: ex
    }));
}

module.exports = {
    create, list, index, replace, update, destroy
};