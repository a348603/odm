const express = require('express');
const AwaitList = require('../models/awaitList');
const Member = require('../models/member');
const Movie = require('../models/movie');

async function create(req, res, next){
    const memberId = req.body.memberIdId;
    const movieId = req.body.movieId;

    let member = await Member.findOne({"_id":memberId});
    let movie = await Movie.findOne({"_id":movieId});
    let awaitList = new AwaitList({
        member: member,
        movie: movie
    });

    awaitList.save().then(obj => res.status(200).json({
        msg: "Lista de espera almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la lista de espera",
        obj: ex
    }));
}

function list(req, res, next) {
    AwaitList.find().populate("_member").then(objs => res.status(200).json({
        msg: "Lista de listas de espera",
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: "No se pudo obtener la lista de listas de espera",
        obj: ex
    }));
}

function index(req, res, next){
    const id = req.params.id;
    AwaitList.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: `Lista de espera con el id ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo consultar la lista de espera",
        obj: ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let member = req.body.member ? req.body.member : "";
    let movie = req.body.movie ? req.body.movie : "";
    
    let awaitList = new Object({
        _member: member,
        _movie: movie
    });
    
    AwaitList.findOneAndUpdate({"_id":id}, awaitList, {new:true})
            .then(obj => res.status(200).json({
                msg: "Lista de espera reemplazada correctamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo reemplazar la lista de espera",
                obj: ex
            }));
}

function update(req, res, next){
    const id = req.params.id;
    let member = req.body.member;
    let movie = req.body.movie;
    
    let awaitList = new Object();
    if(member) awaitList._member = member;
    if(movie) awaitList._movie = movie;
    
    AwaitList.findOneAndUpdate({"_id":id}, awaitList)
            .then(obj => res.status(200).json({
                msg: "Lista de espera actualizada corretamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo actualizar la lista de espera",
                obj: ex
            }));
}

function destroy(req, res, next){
    const id = req.params.id;
    AwaitList.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg: "Lista de espera eliminada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo eliminar la lista de espera",
        obj: ex
    }));
}

module.exports = {
    create, list, index, replace, update, destroy
};