const exports = require('express')
const user = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const JwtKey ="e062dcb0bf3b2ab1bb1d1365a6fc81ed";
function home(req, res, next) {
    res.render('index', { title: 'Express'});
}

function login(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    user.findOne({"_email": email}).then(user => {
        if (user) {
            bcrypt.hash(password, user.salt, (err, hash) => {
                if(err){
                    res.status(403).json({
                        msg: "Usuario y/o contraseña incorrecto",
                        obj: err
                    });
                }
                if(hash === user.password){
                    res.status(200).json({
                        msg: "Login ok",
                        obj: jwt.sign({data:user.data, exp: Math.floor(Date.now()/1000)+600}, JwtKey)
                    });
                }else {
                    res.status(403).json({
                        msg: "Usuario y/o contraseña incorrecto",
                        obj: null
                    });
                }
            })
        }else {
            res.status(403).json({
                msg: "Usuario y/o contraseña incorrecto",
                obj: null
            });
        }
    }).catch(ex => res.status(403).json({
        msg: "Usuario y/o contraseña incorrecto",
        obj: ex
    }));
}

module.exports = {home, login}