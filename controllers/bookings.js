const express = require('express');
const Booking = require('../models/booking');
const Member = require('../models/member');
const Copy = require('../models/copy');

async function create(req, res, next){
    const date = req.body.date;
    const memberId = req.body.memberIdId;
    const copyId = req.body.copyId;

    let member = await Member.findOne({"_id":memberId});
    let copy = await Copy.findOne({"_id":copyId});
    let booking = new Booking({
        date: date,
        member: member,
        copy: copy
    });

    booking.save().then(obj => res.status(200).json({
        msg: "Reserva almacenada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo crear la reserva",
        obj: ex
    }));
}

function list(req, res, next) {
    Booking.find().populate("_member").then(objs => res.status(200).json({
        msg: "Lista de reservas",
        obj: objs
    })).catch(ex => res.status(500).json({
        msg: "No se pudo obtener la lista de reservas",
        obj: ex
    }));
}

function index(req, res, next){
    const id = req.params.id;
    Booking.findOne({"_id":id}).then(obj => res.status(200).json({
        msg: `Reserva con el id ${id}`,
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo consultar la reserva",
        obj: ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    let date = req.body.date ? req.body.date : "";
    let member = req.body.member ? req.body.member : "";
    let copy = req.body.copy ? req.body.copy : "";
    
    let booking = new Object({
        _date: date,
        _member: member,
        _copy: copy
    });
    
    Booking.findOneAndUpdate({"_id":id}, booking, {new:true})
            .then(obj => res.status(200).json({
                msg: "Reserva reemplazada correctamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo reemplazar la reserva",
                obj: ex
            }));
}

function update(req, res, next){
    const id = req.params.id;
    let date = req.body.date;
    let member = req.body.member;
    let copy = req.body.copy;
    
    let booking = new Object();
    if(date) booking._date = date;
    if(member) booking._member = member;
    if(copy) booking._copy = copy;
    
    Booking.findOneAndUpdate({"_id":id}, booking)
            .then(obj => res.status(200).json({
                msg: "Reserva actualizada corretamente",
                obj: obj
            })).catch(ex => res.status(500).json({
                msg: "No se pudo actualizar la reserva",
                obj: ex
            }));
}

function destroy(req, res, next){
    const id = req.params.id;
    Booking.findByIdAndRemove({"_id":id}).then(obj => res.status(200).json({
        msg: "Reserva eliminada correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        msg: "No se pudo eliminar la reserva",
        obj: ex
    }));
}

module.exports = {
    create, list, index, replace, update, destroy
};